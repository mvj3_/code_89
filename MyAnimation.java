package cn.oce.youku;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.RotateAnimation;

public class MyAnimation {
	//Èë¶¯»­
	public static void startAnimationIN(ViewGroup viewGroup, int duration){
		for(int i = 0; i < viewGroup.getChildCount(); i++ ){
			viewGroup.getChildAt(i).setVisibility(View.VISIBLE);//ÉèÖÃÏÔÊ¾
			viewGroup.getChildAt(i).setFocusable(true);//»ñµÃ½¹µã
			viewGroup.getChildAt(i).setClickable(true);//¿ÉÒÔµã»÷
		}
		
		Animation animation;
		/**
		 * Ðý×ª¶¯»­
		 * RotateAnimation(fromDegrees, toDegrees, pivotXType, pivotXValue, pivotYType, pivotYValue)
		 * fromDegrees ¿ªÊ¼Ðý×ª½Ç¶È
		 * toDegrees Ðý×ªµ½µÄ½Ç¶È
		 * pivotXType XÖá ²ÎÕÕÎï
		 * pivotXValue xÖá Ðý×ªµÄ²Î¿¼µã
		 * pivotYType YÖá ²ÎÕÕÎï
		 * pivotYValue YÖá Ðý×ªµÄ²Î¿¼µã
		 */
		animation = new RotateAnimation(-180, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 1.0f);
		animation.setFillAfter(true);//Í£ÁôÔÚ¶¯»­½áÊøÎ»ÖÃ
		animation.setDuration(duration);
		
		viewGroup.startAnimation(animation);
		
	}
	
	//³ö¶¯»­
	public static void startAnimationOUT(final ViewGroup viewGroup, int duration , int startOffSet){
		Animation animation;
		animation = new RotateAnimation(0, -180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 1.0f);
		animation.setFillAfter(true);//Í£ÁôÔÚ¶¯»­½áÊøÎ»ÖÃ
		animation.setDuration(duration);
		animation.setStartOffset(startOffSet);
		animation.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				for(int i = 0; i < viewGroup.getChildCount(); i++ ){
					viewGroup.getChildAt(i).setVisibility(View.GONE);//ÉèÖÃÏÔÊ¾
					viewGroup.getChildAt(i).setFocusable(false);//»ñµÃ½¹µã
					viewGroup.getChildAt(i).setClickable(false);//¿ÉÒÔµã»÷
				}
				
			}
		});
		
		viewGroup.startAnimation(animation);
	}
}
